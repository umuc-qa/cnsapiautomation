$FirefoxUpload = "File Upload"
$ChromeUpload = "Open"
$IEUpload = "Choose File to Upload"
If WinExists($FirefoxUpload) Then
   $UploadWindow = $FirefoxUpload
Elseif WinExists($ChromeUpload) Then
   $UploadWindow = $ChromeUpload
else
   $UploadWindow = $IEUpload
EndIf

ControlClick($UploadWindow,"","Edit1")
ControlSetText($UploadWindow,"","Edit1",@WorkingDir&"\AutoIt\Data\project_1_introduction.html")
ControlClick($UploadWindow,"","Button1")
