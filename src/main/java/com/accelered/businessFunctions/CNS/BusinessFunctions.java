package com.accelered.businessFunctions.CNS;

import java.io.BufferedReader;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.testng.Assert;

import com.accelered.accelerators.ActionEngine;
import com.accelered.accelerators.RESTWebServices;
import com.accelered.utilities.*;

import io.restassured.response.Response;

public class BusinessFunctions extends ActionEngine {

	public void validateEnrollments(String baseUrl, String username, String password, String progVersionNameKey,
			String campusNameKey, String progVersionValue, String campusNameValue) throws Throwable {
		Response enrollmentResponse = null;
		String progVersionFromAPI = null;
		String campusNameFromAPI = null;
		Map<String, String> hmapEnrollments = new HashMap<String, String>();
		ArrayList<HashMap<String, String>> arrayListEnrollments = new ArrayList<HashMap<String, String>>();

		String cryptoKey = "Mary has one cat";
		String passwordCleartext = Crypto.decryptString(password, cryptoKey);

		try {
			enrollmentResponse = RESTWebServices.CNS_GET_Request(baseUrl, username, passwordCleartext);
			SuccessReport("EnrollmentAPIResponse", enrollmentResponse.asString());

			arrayListEnrollments = enrollmentResponse.jsonPath().get("value");

			for (int i = 0; i < arrayListEnrollments.size(); i++) {
				hmapEnrollments = arrayListEnrollments.get(i);
				progVersionFromAPI = hmapEnrollments.get(progVersionNameKey);
				campusNameFromAPI = hmapEnrollments.get(campusNameKey);
			}
			if (progVersionFromAPI.trim().equals(progVersionValue)) {
				SuccessReport("Expected ProgramVersionName", progVersionValue);
				SuccessReport("Actual ProgramVersionName", progVersionFromAPI);
			} else {
				failureReport("Expected ProgramVersionName", progVersionValue);
				failureReport("Actual ProgramVersionName", progVersionFromAPI);
			}
			if (campusNameFromAPI.trim().equals(campusNameValue)) {
				SuccessReport("Expected CampusName", campusNameValue);
				SuccessReport("Actual CampusName", campusNameFromAPI);
			} else {
				failureReport("Expected CampusName", campusNameValue);
				failureReport("Actual CampusName", campusNameFromAPI);
				//Assert.assertTrue(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void validateStudentDocs(String baseUrl, String username, String password, String docTypeKey,
			String reqDateKey, String dueDateKey, String docStatusKey, String docTypeValue, String reqDateValue,
			String dueDateValue, String docStatusValue) throws Throwable {
		Response studentDocsResponse = null;

		String docTypeFromAPI = null;
		String reqDateFromAPI = null;
		String docDueDateFromAPI = null;
		String docStatusFromAPI = null;

		String docType = null;
		String reqDate = null;
		String docDueDate = null;
		String docStatus = null;

		ArrayList<String> arrayListDocType = new ArrayList<String>();
		ArrayList<String> arrayListRedDate = new ArrayList<String>();
		ArrayList<String> arrayListDueDate = new ArrayList<String>();
		ArrayList<String> arrayListDocStatus = new ArrayList<String>();

		BufferedReader readerDocType = new BufferedReader(new StringReader(docTypeValue));
		BufferedReader readerReqDate = new BufferedReader(new StringReader(reqDateValue));
		BufferedReader readerDueDate = new BufferedReader(new StringReader(dueDateValue));
		BufferedReader readerDocStatus = new BufferedReader(new StringReader(docStatusValue));

		while ((docType = readerDocType.readLine()) != null) {
			arrayListDocType.add(docType);
		}
		while ((reqDate = readerReqDate.readLine()) != null) {
			arrayListRedDate.add(reqDate);
		}
		while ((docDueDate = readerDueDate.readLine()) != null) {
			arrayListDueDate.add(docDueDate);
		}
		while ((docStatus = readerDocStatus.readLine()) != null) {
			arrayListDocStatus.add(docStatus);
		}

		Map<String, String> hmapStudentDocs = new HashMap<String, String>();

		ArrayList<HashMap<String, String>> arrayListStudentDocs = new ArrayList<HashMap<String, String>>();

		String cryptoKey = "Mary has one cat";
		String passwordCleartext = Crypto.decryptString(password, cryptoKey);

		try {
			studentDocsResponse = RESTWebServices.CNS_GET_Request(baseUrl, username, passwordCleartext);
			SuccessReport("StudentDocumentsAPIResponse", studentDocsResponse.asString());

			arrayListStudentDocs = studentDocsResponse.jsonPath().get("value");

			for (int i = 0; i < arrayListStudentDocs.size(); i++) {
				hmapStudentDocs = arrayListStudentDocs.get(i);

				// docTypeFromAPI = hmapStudentDocs.get(docTypeKey);
				// arrayListDocTypeFromAPI.add(docTypeFromAPI);

				reqDateFromAPI = hmapStudentDocs.get(reqDateKey);
				reqDateFromAPI = reqDateFromAPI.substring(0, 10);
				// arrayListRedDateFromAPI.add(reqDateFromAPI);

				docDueDateFromAPI = hmapStudentDocs.get(dueDateKey);
				docDueDateFromAPI = docDueDateFromAPI.substring(0, 10);
				// arrayListDueDateFromAPI.add(docDueDateFromAPI);

				// docStatusFromAPI = hmapStudentDocs.get(docStatusKey);
				// arrayListDocStatusFromAPI.add(docStatusFromAPI);

				if (reqDateFromAPI.contains(arrayListRedDate.get(i))) {
					SuccessReport("Expected Requested Date", arrayListRedDate.get(i));
					SuccessReport("Actual Requested Date", reqDateFromAPI);
				} else {
					failureReport("Expected Requested Date", arrayListRedDate.get(i));
					failureReport("Actual Requested Date", reqDateFromAPI);
				}

				if (docDueDateFromAPI.contains(arrayListDueDate.get(i))) {
					SuccessReport("Expected Due Date", arrayListDueDate.get(i));
					SuccessReport("Actual Due Date", docDueDateFromAPI);
				} else {
					failureReport("Expected Due Date", arrayListDueDate.get(i));
					failureReport("Actual Due Date", docDueDateFromAPI);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void ValidateGetRegistrationLocksDetails(String baseUrl, String username, String password,
			String validationKey1, String expectedValue1, String validationKey2, String expectedValue2)
			throws Throwable {
		Response responseGetRegistrationLocksDetails = null;
		String lockTypeNameValue = null;
		String termTypeValue = null;
		String lockTypeNameKey = null;
		String termTypeKey = null;
		String lockTypeNameFromAPI = null;
		String termTypeFromAPI = null;
		boolean bResponses;

		Map<String, String> hmapRegistrationLocksDetails = new HashMap<String, String>();
		ArrayList<HashMap<String, String>> arrayListRegistrationLocksDetails = new ArrayList<HashMap<String, String>>();

		try {
			String cryptoKey = "Mary has one cat";
			String passwordCleartext = Crypto.decryptString(password, cryptoKey);

			responseGetRegistrationLocksDetails = RESTWebServices.CNS_GET_Request(baseUrl, username, passwordCleartext);
			// bResponses = true;
			SuccessReport("RegistrationLocksDetailsAPIResponse", responseGetRegistrationLocksDetails.asString());

			lockTypeNameValue = (!isNullOrEmpty(expectedValue1) ? expectedValue1 : null);
			termTypeValue = (!isNullOrEmpty(expectedValue2) ? expectedValue2 : null);
			lockTypeNameKey = (!isNullOrEmpty(validationKey1) ? validationKey1 : null);
			termTypeKey = (!isNullOrEmpty(validationKey2) ? validationKey2 : null);
			arrayListRegistrationLocksDetails = responseGetRegistrationLocksDetails.jsonPath().get("value");

			for (int i = 0; i < arrayListRegistrationLocksDetails.size(); i++) {
				hmapRegistrationLocksDetails = arrayListRegistrationLocksDetails.get(i);
				lockTypeNameFromAPI = hmapRegistrationLocksDetails.get(lockTypeNameKey);
				termTypeFromAPI = hmapRegistrationLocksDetails.get(termTypeKey);
			}
			if (lockTypeNameFromAPI.trim().equals(lockTypeNameValue)) {
				SuccessReport("Expected Lock Type Name", lockTypeNameValue);
				SuccessReport("LockTypeName", lockTypeNameFromAPI);
			} else {
				failureReport("Expected Lock Type Name", lockTypeNameValue);
				failureReport("LockTypeName", lockTypeNameFromAPI);
			}
			if (termTypeFromAPI.trim().equals(termTypeValue)) {
				SuccessReport("Expected Term Type", termTypeValue);
				SuccessReport("Actual Term Type", termTypeFromAPI);
			} else {
				failureReport("Expected Term Type", termTypeValue);
				failureReport("Actual Term Type", termTypeFromAPI);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isNullOrEmpty(String _str) {
		if (_str == null || _str.length() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
