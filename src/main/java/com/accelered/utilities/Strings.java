package com.accelered.utilities;

public class Strings {

	public static Boolean isNullOrEmpty(String _str)
	{
		return _str == null || _str.length() == 0;
	}
}
