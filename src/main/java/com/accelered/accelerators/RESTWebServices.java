package com.accelered.accelerators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.SSLConfig;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.internal.RestAssuredResponseImpl;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class RESTWebServices extends ActionEngine {

	public static Response CNS_GET_Request(String baseUrl, String username, String passwordCleartext) {
		RequestSpecification httpRequest = null;
		try {
			// Required to overcome or bypass any SSLException: hostname in certificate
			// didn't match
			RestAssured.config = RestAssured.config().sslConfig(SSLConfig.sslConfig().allowAllHostnames());
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given().auth().preemptive().basic(username, passwordCleartext);
			Response response = httpRequest.request(Method.GET);

			System.out.println(
					"-----------------------------------------GET-----------------------------------------------------");
			System.out.println("---STATUS---");
			System.out.println(response.getStatusCode());
			System.out.println("---BODY---");
			System.out.println(response.getBody().asString());

			if (response.statusCode() == 200) {
				return response;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

}// Class
