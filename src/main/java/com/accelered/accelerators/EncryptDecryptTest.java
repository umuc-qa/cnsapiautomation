package com.accelered.accelerators;

import com.accelered.utilities.*;

public class EncryptDecryptTest {

    public static void main(String [] args) throws Exception {
        // String username = "rajesh.kothakota@umuc.edu";
        String password = "marylandPWD@21";

        try {
    		String cryptoKey = "Mary has one cat";
    		String passwordEncText = Crypto.encryptString(password, cryptoKey);
    		String passwordClearText = Crypto.decryptString(passwordEncText, cryptoKey);

    		System.out.println("Encrypted password: " + passwordEncText);
    		System.out.println("Decrypted password: " + passwordClearText);
        }
        catch (Exception e) {
        	System.out.println("Encrypt/Decrypt exception: " + e.getMessage());
        }
    }
}
