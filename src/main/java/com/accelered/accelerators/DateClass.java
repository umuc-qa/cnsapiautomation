package com.accelered.accelerators;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;


public class DateClass {

	public static void main(String[] args) throws ParseException, FilloException{
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		
		System.out.println(dateLesserGreater("29-04-2018", dateFormat.format(date), "dd-MM-yyyy"));
		System.out.println(dateLesserGreater("08-05-2018", dateFormat.format(date), "dd-MM-yyyy"));
		
		Fillo fillo = new Fillo();
		Connection dates = fillo.getConnection("Data\\workday\\TestData.xls");
		String queryDates = "Select * from getPPDate";
		Recordset dt = dates.executeQuery(queryDates);
		try {
			//dt = dates.executeQuery("Select * from Dates1");
			System.out.println(dt.getCount());
			while(dt.next()){
				String ppStDate=dt.getField("Pay Period Start Date");
				String ppEdDate=dt.getField("Pay Period End Date");
				String mondayDate=dt.getField("MondayDate");
				System.out.println(mondayDate);
			}
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static int dateDifference(String date1, String date2, String dtFormat){
		String diff = "";
		try {
				String format = dtFormat;
				SimpleDateFormat sdf = new SimpleDateFormat(format);
		        Date fromDate = sdf.parse(date1);
		        Date toDate = sdf.parse(date2);
				long timeDiff = Math.abs(fromDate.getTime() - toDate.getTime());        
				diff = String.format("%d hour(s) %d min(s)", TimeUnit.MILLISECONDS.toHours(timeDiff), TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff)));
			 
				//System.out.println(Integer.parseInt(diff.split(" ")[0])/24);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.parseInt(diff.split(" ")[0])/24;
	}
	
	
	
	public static int dateLesserGreater(String date1, String date2, String dtFormat){
		int diff = 0;
		try {
				String format = dtFormat;
				SimpleDateFormat sdf = new SimpleDateFormat(format);
		        Date fromDate = sdf.parse(date1);
		        Date toDate = sdf.parse(date2);
				diff= fromDate.compareTo(toDate);// - toDate.getHours());        
				//diff = ("%d hour(s) %d min(s)", TimeUnit.MILLISECONDS.toHours(timeDiff), TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff)));
			 
				//System.out.println(Integer.parseInt(diff.split(" ")[0])/24);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return diff;//Integer.parseInt(diff.split(" ")[0])/24;
	}
	

}


