package com.accelered.tests.CNS;

import org.testng.annotations.Test;

import com.accelered.businessFunctions.CNS.BusinessFunctions;
import com.accelered.utilities.Data_Provider;

public class CNSAPI_UC03_StudentDocs extends BusinessFunctions {
	@Test
	public void CNSAPI_UC03_StudentDocs() throws Throwable {
		try {

			tstData = Data_Provider.getTestData("CNS", "CNSAPI_UC03_StudentDocs");
			if (tstData != null) {
				String baseUrl = tstData.get("BaseUrl");
				String username = tstData.get("Username");
                String password = tstData.get("Password");
				String docTypeKey = tstData.get("ValidationKey1");
				String reqDateKey = tstData.get("ValidationKey2");
				String dueDateKey = tstData.get("ValidationKey3");
				String docStatusKey = tstData.get("ValidationKey4");
				String docTypeValue = tstData.get("ValidationValue1");
				String reqDateValue = tstData.get("ValidationValue2");
				String dueDateValue = tstData.get("ValidationValue3");
				String docStatusValue = tstData.get("ValidationValue4");
				String studentId = tstData.get("StudentId");

				baseUrl = baseUrl.replace("{studentId}", studentId);

				validateStudentDocs(baseUrl,username, password, docTypeKey, reqDateKey, dueDateKey, docStatusKey, docTypeValue,
						reqDateValue, dueDateValue, docStatusValue);
			}
		} catch (Exception e) {
			e.printStackTrace();
			failureReport("Execution Status", "Script not executed completely");
		}
	}
}
