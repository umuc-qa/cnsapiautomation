package com.accelered.tests.CNS;

import org.testng.annotations.Test;

import com.accelered.businessFunctions.CNS.BusinessFunctions;
import com.accelered.utilities.Data_Provider;

public class CNSAPI_UC02_Enrollments extends BusinessFunctions{
	@Test
	public void CNSAPI_UC02_Enrollments() throws Throwable {
	try {
		
		tstData = Data_Provider.getTestData("CNS", "CNSAPI_UC02_Enrollments");
		if(tstData != null) {
		String baseUrl = tstData.get("BaseUrl");
		String username = tstData.get("Username");
        String password = tstData.get("Password");
		String progVersionNameKey = tstData.get("ValidationKey1");
		String campusNameKey = tstData.get("ValidationKey2");	
		String progVersionNameValue = tstData.get("ValidationValue1");
		String campusNameValue = tstData.get("ValidationValue2");
		String studentId = tstData.get("StudentId");
		
		baseUrl = baseUrl.replace("{studentId}", studentId);
				
		validateEnrollments(baseUrl,username, password,progVersionNameKey,campusNameKey,progVersionNameValue,campusNameValue);	
		}
	}	
	catch (Exception e) {
		e.printStackTrace();
		failureReport("Execution Status", "Script not executed completely"); 
	}
	}
}



