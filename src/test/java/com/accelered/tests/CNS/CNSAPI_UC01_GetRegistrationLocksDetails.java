package com.accelered.tests.CNS;

import org.testng.annotations.Test;

import com.accelered.businessFunctions.CNS.BusinessFunctions;
import com.accelered.utilities.Data_Provider;

public class CNSAPI_UC01_GetRegistrationLocksDetails extends BusinessFunctions {

	@Test
    public void CNSAPI_UC01_GetRegistrationLocksDetails() throws Throwable {
        try {
            tstData = Data_Provider.getTestData("CNS", "CNSAPI_UC01_GetRegistrationLocksDetails");
            if(tstData != null) {
                String baseUrl = tstData.get("BaseUrl");
                String basePath = tstData.get("BasePath");
                String studentId = tstData.get("StudentId");
                String username = tstData.get("Username");
                String password = tstData.get("Password");
                String validationKey1 = tstData.get("ValidationKey1");
                String validationValue1 = tstData.get("ValidationValue1");
                String validationKey2 = tstData.get("ValidationKey2");
                String validationValue2 = tstData.get("ValidationValue2");                        
                String baseUrlStudentId = baseUrl.replace("{StudentId}", studentId);
                
                ValidateGetRegistrationLocksDetails(baseUrlStudentId, username, password, validationKey1, validationValue1, validationKey2, validationValue2);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            failureReport("Execution Status", "Script not executed completely");
        }
	}
}
